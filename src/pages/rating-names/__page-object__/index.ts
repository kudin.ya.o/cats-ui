import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';

export class RatingPage {
  private page: Page;
  public errorPopUpSelector: string;
  public likesCountSelector: string;

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
    this.errorPopUpSelector = '//div[contains(@class,"alertify-notifier")]';
    this.likesCountSelector = '.rating-names_item-count__1LGDH.has-text-success';
  }

  async mockApiResponse(mock) {
    return await test.step('Мокирование ответа сервера', async () => {
      await this.page.route (
        request => request.href.includes('/cats/rating'),
        async route => {
          route.fulfill(mock);
        });
    })
  }

  async openRatingPage() {
    return await test.step('Открытие страницы рейтинга котиков', async () => {
      await this.page.goto('/rating')
    })
  }

  async getLikesCounts(): Promise<number[]> {
    return await test.step('Получение количества лайков', async () => {
      const likeCounts = await this.page.locator(this.likesCountSelector).allTextContents();
      return likeCounts.map(el => parseInt(el.trim(), 10));
    });
  }
}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};




