export const errorResponse = {
  status: 500,
  contentType: 'application/json',
  body: JSON.stringify({
    error: 'Internal Server Error',
    message: 'Ошибка загрузки рейтинга'
  })
};