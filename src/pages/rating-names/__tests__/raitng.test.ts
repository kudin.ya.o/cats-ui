import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__';
import { errorResponse } from '../__mocks__/error'; 

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage }) => {
  
  // Замокать ответ метода получения рейтинга ошибкой на стороне сервера
  await ratingPage.mockApiResponse(errorResponse);

  // Перейти на страницу рейтинга
  await ratingPage.openRatingPage();

  // Проверить, что отображается текст ошибка загрузки рейтинга
   expect( await page.locator(ratingPage.errorPopUpSelector).textContent()).toContain('Ошибка загрузки рейтинга');
  // await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается по убыванию', async ({ page, ratingPage }) => {

  // Перейти на страницу рейтинга
  await ratingPage.openRatingPage();

  // Проверить, что рейтинг количества лайков отображается по убыванию

  //Получить массив лайков
  const likeCounts = await ratingPage.getLikesCounts();

  //Проверить, что массив отсортирован по убыванию
  const sortedLikeCounts = [...likeCounts].sort((a, b) => b - a);
  expect(likeCounts).toEqual(sortedLikeCounts);
});

